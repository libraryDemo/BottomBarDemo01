package com.cqc.bottombardemo01;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.BottomBarTab;
import com.roughike.bottombar.OnTabReselectListener;
import com.roughike.bottombar.OnTabSelectListener;

public class BadgesActivity extends AppCompatActivity {

    private BottomBar bottomBar;
    private TextView tv;
    private Toast toast;
    private BottomBarTab tab3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample);


        toast = Toast.makeText(BadgesActivity.this, "", Toast.LENGTH_SHORT);

        findViews();
        initViews();
    }

    /**
     * 选中的tab的icon+title的颜色是  colorPrimary
     */
    private void initViews() {
        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                switch (tabId) {
                    case R.id.tab1:
                        toast.setText("tab1");
                        toast.show();
                        break;
                    case R.id.tab2:
                        toast.setText("tab2");
                        toast.show();
                        break;
                    case R.id.tab3:
                        toast.setText("tab3");
                        toast.show();
                        tab3.removeBadge();
                        break;
                    case R.id.tab4:
                        toast.setText("tab4");
                        toast.show();
                        break;
                    case R.id.tab5:
                        toast.setText("tab5");
                        toast.show();
                        break;
                }
            }
        });

        //当前的tab是tab1，而你又点击了tab1，会调用这个方法
        bottomBar.setOnTabReselectListener(new OnTabReselectListener() {
            @Override
            public void onTabReSelected(@IdRes int tabId) {
                switch (tabId) {
                    case R.id.tab1:
                        toast.setText("onTabReSelected---tab1");
                        toast.show();
                        break;
                    case R.id.tab2:
                        toast.setText("onTabReSelected---tab2");
                        toast.show();
                        break;
                    case R.id.tab3:
                        toast.setText("onTabReSelected---tab3");
                        toast.show();
                        break;
                    case R.id.tab4:
                        toast.setText("onTabReSelected---tab4");
                        toast.show();
                        break;
                    case R.id.tab5:
                        toast.setText("onTabReSelected---tab5");
                        toast.show();
                        break;
                }
            }
        });

        tab3.setBadgeCount(5);
    }

    private void findViews() {
        bottomBar = (BottomBar) findViewById(R.id.bottomBar);
        tab3 = bottomBar.getTabWithId(R.id.tab3);
        tv = (TextView) findViewById(R.id.tv);

    }
}
