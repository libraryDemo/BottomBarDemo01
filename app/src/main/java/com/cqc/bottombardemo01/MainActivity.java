package com.cqc.bottombardemo01;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by ${cqc} on 2016/12/5.
 */

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btn1;
    private Button btn2;
    private Button btn3;
    private Button btn4;
    private Button btn5;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViews();
        initViews();
    }

    private void initViews() {
        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
        btn5.setOnClickListener(this);
    }

    private void findViews() {
        btn1 = (Button) findViewById(R.id.btn1);
        btn2 = (Button) findViewById(R.id.btn2);
        btn3 = (Button) findViewById(R.id.btn3);
        btn4 = (Button) findViewById(R.id.btn4);
        btn5 = (Button) findViewById(R.id.btn5);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn1:
                startActivity(new Intent(MainActivity.this, SampleActivity.class));
                break;
            case R.id.btn2:
                startActivity(new Intent(MainActivity.this, BottomBarColorActivity.class));
                break;
            case R.id.btn3:
                startActivity(new Intent(MainActivity.this, CustomColorAndFontActivity.class));
                break;
            case R.id.btn4:
                startActivity(new Intent(MainActivity.this, ScrollBottomBarDismissActivity.class));
                break;
            case R.id.btn5:
                startActivity(new Intent(MainActivity.this, BadgesActivity.class));
                break;
        }
    }
}
